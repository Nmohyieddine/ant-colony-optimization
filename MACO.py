from ACO import ACO


class MACO:
    def __init__(self, ant_count, generations, alpha, beta, rho, q, strategy,number_aco):

        self.Q = q
        self.rho = rho
        self.beta = beta
        self.alpha = alpha
        self.ant_count = ant_count
        self.generations = generations
        self.update_strategy = strategy
        self.number_aco = number_aco


    def update_pheromone(self, graph, result):
        for i, row in enumerate(graph.shared_pheromone):
            for j, col in enumerate(row):
                graph.shared_pheromone[i][j] *= self.rho
                graph.shared_pheromone[i][j] += result[i][j]




    def solve(self, graph):
        acos = []
        best_solution = []
        for _ in range(self.number_aco):
            acos.append(ACO(self.ant_count,self.generations,self.alpha,self.beta,self.rho,self.Q,self.update_strategy))

        for _ in range(self.generations):
            best_cost = float('inf')

            for aco in acos:
                result_aco, best_solution_aco, best_cost_aco = aco.solve_maco(graph)

                if best_cost_aco < best_cost:
                    best_cost = best_cost_aco
                    best_solution = [] + best_solution_aco
                    result = result_aco

            self.update_pheromone(graph, result)

        return best_solution, best_cost