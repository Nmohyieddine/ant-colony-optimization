import numpy as np
class Graph:

    def __init__(self, citys):
        self.cost_matrix = calculCost(citys)[1]
        self.city_number = len(citys)
        self.pheromone = np.full((self.city_number,self.city_number), 1/self.city_number*self.city_number)
        self.shared_pheromone = np.full((self.city_number,self.city_number), 0)
        self.point = calculCost(citys)[0]
        self.city_name = getCityName(citys)

def distance(c1,c2):
    (x1,y1),(x2,y2) = (c1.coordonne, c2.coordonne)
    return np.sqrt((x1-x2)**2+(y1-y2)**2)
def calculCost(citys):
    cost_matrix = []
    points = []
    for i in range(len(citys)):
        row = []
        points.append(citys[i].coordonne)
        for j in range(len(citys)):
            row.append(distance(citys[i], citys[j]))
        cost_matrix.append(row)
    return points,cost_matrix

def getCityName(citys):
    citys_name = []
    for i in range(len(citys)):
        citys_name.append(citys[i].name)

    return citys_name
