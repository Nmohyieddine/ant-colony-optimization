import matplotlib.pyplot as plt
import numpy as np

from ACO import ACO
from City import City
from MACO import MACO
from Plot import *
import time
import pandas as pd



from Graph import Graph

_CITIESSIZE = 5
_CITIES = []
df = pd.read_csv('fr.csv')
citys = df[['city','lat','lng']]
print(citys)
for index, row in df.iterrows():
    #print(row['c1'], row['c2'])
    _CITIES.append((row['city'], (row['lat'], row['lng'])))
    if _CITIESSIZE -1 == index:
        break

_CITIES = tuple(_CITIES)
print(len(_CITIES))



RANOMSEED = 12345

NBFOURMIS = 100
NBEPOCHS = 500
NBVILLES = len(_CITIES)
ALPHA = 0.6
BETA = 0.4
EVAPORATION = 0.8
q = 10
COEFFEXPLORATION = 0.05
Q = 1 / NBVILLES
NUMBRE_ACO = 10



def prepareCitys(_cities):
    citys = []
    for i in range(NBVILLES):
        citys.append(City(_cities[i][0], _cities[i][1]))
    return citys





def main():



    citys = prepareCitys(_CITIES)
    graph_maco = Graph(citys)
    start = time.time()


    #aco = ACO(NBFOURMIS,NBEPOCHS,ALPHA,BETA,EVAPORATION,q,2)
    maco = MACO(NBFOURMIS,NBEPOCHS,ALPHA,BETA,EVAPORATION,q,2,NUMBRE_ACO)
    sol, cost = maco.solve(graph_maco)
    plot(graph_maco,sol,"MACO")
    end = time.time()
    time_maco = end - start

    start = time.time()
    graph_aco = Graph(citys)
    aco = ACO(NBFOURMIS,NBEPOCHS,ALPHA,BETA,EVAPORATION,q,2)
    sol, cost = aco.solve(graph_aco)
    plot(graph_aco,sol, "ACO")
    end = time.time()
    time_aco = end - start


    names = ['MACO', 'ACO']
    values = [time_maco, time_aco]
    plt.bar(names, values);
    plt.ylabel('Temps')
    plt.title(f'Durée déxécution pour {_CITIESSIZE} villes')
    plt.savefig("Durée_exécution.png")
    plt.show()

if __name__== '__main__':
    main()