import random
class Ant:
    def __init__(self, aco, graph):
        self.colony = aco
        self.graph = graph
        self.total_cost = 0.0
        self.tabu = []  # tabu list
        self.pheromone_delta = []  # the local increase of pheromone
        self.allowed = [i for i in range(graph.city_number)]  # nodes which are allowed for the next selection
        self.eta = [[0 if i == j else 1 / graph.cost_matrix[i][j] for j in range(graph.city_number)] for i in
                    range(graph.city_number)]  # heuristic information
        start = random.randint(0, graph.city_number - 1)  # start from any node
        self.tabu.append(start)
        self.current = start
        self.allowed.remove(start)

    def _select_next(self):
        denominator = 0
        for i in self.allowed:
            denominator += self.graph.pheromone[self.current][i] ** self.colony.alpha * self.eta[self.current][
                                                                                            i] ** self.colony.beta
        # noinspection PyUnusedLocal
        probabilities = [0 for i in range(self.graph.city_number)]  # probabilities for moving to a node in the next step
        for i in range(self.graph.city_number):
            try:
                self.allowed.index(i)  # test if allowed list contains i
                probabilities[i] = self.graph.pheromone[self.current][i] ** self.colony.alpha * \
                    self.eta[self.current][i] ** self.colony.beta / denominator
            except ValueError:
                pass  # do nothing
        # select next node by probability roulette
        selected = 0
        rand = random.random()
        for i, probability in enumerate(probabilities):
            rand -= probability
            if rand <= 0:
                selected = i
                break
        self.allowed.remove(selected)
        self.tabu.append(selected)
        self.total_cost += self.graph.cost_matrix[self.current][selected]
        self.current = selected

    def _select_next_maco(self):
        denominator = 0
        for i in self.allowed:
            # TODO: here i changed pheromone(local) with shared_pheromone
            denominator += self.graph.shared_pheromone[self.current][i] ** self.colony.alpha * self.eta[self.current][
                i] ** self.colony.beta
        # noinspection PyUnusedLocal
        probabilities = [0 for i in
                         range(self.graph.city_number)]  # probabilities for moving to a node in the next step
        for i in range(self.graph.city_number):
            try:
                self.allowed.index(i)  # test if allowed list contains i
                probabilities[i] = self.graph.pheromone[self.current][i] ** self.colony.alpha * \
                                   self.eta[self.current][i] ** self.colony.beta / denominator
            except ValueError:
                pass  # do nothing
        # select next node by probability roulette
        selected = 0
        #rand = random.random()
        max_prob = float('-inf')
        for i, probability in enumerate(probabilities):
            #rand -= probability
            if max_prob <= probability:
                selected = i
                max_prob = probability
        self.allowed.remove(selected)
        self.tabu.append(selected)
        self.total_cost += self.graph.cost_matrix[self.current][selected]
        self.current = selected

    # noinspection PyUnusedLocal
    def _update_pheromone_delta(self):
        self.pheromone_delta = [[0 for j in range(self.graph.city_number)] for i in range(self.graph.city_number)]
        for _ in range(1, len(self.tabu)):
            i = self.tabu[_ - 1]
            j = self.tabu[_]
            if self.colony.update_strategy == 1:  # ant-quality system
                self.pheromone_delta[i][j] = self.colony.Q
            elif self.colony.update_strategy == 2:  # ant-density system
                # noinspection PyTypeChecker
                self.pheromone_delta[i][j] = self.colony.Q / self.graph.cost_matrix[i][j]
            else:  # ant-cycle system
                self.pheromone_delta[i][j] = self.colony.Q / self.total_cost
