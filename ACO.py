from cmath import inf

from Ant import Ant


class ACO:
    def __init__(self, ant_count, generations, alpha, beta, rho, q, strategy):

        self.Q = q
        self.rho = rho
        self.beta = beta
        self.alpha = alpha
        self.ant_count = ant_count
        self.generations = generations
        self.update_strategy = strategy


    def _update_pheromone(self, graph, ants):
        for i, row in enumerate(graph.pheromone):
            for j, col in enumerate(row):
                graph.pheromone[i][j] *= self.rho
                for ant in ants:
                    graph.pheromone[i][j] += ant.pheromone_delta[i][j]
        return graph.pheromone


    # noinspection PyProtectedMember
    def solve(self, graph):

        best_cost = float('inf')
        best_solution = []
        for _ in range(self.generations):
            # noinspection PyUnusedLocal
            ants = [Ant(self, graph) for i in range(self.ant_count)]
            for ant in ants:
                for _ in range(graph.city_number - 1):
                    ant._select_next()
                ant.total_cost += graph.cost_matrix[ant.tabu[-1]][ant.tabu[0]]
                if ant.total_cost < best_cost:
                    best_cost = ant.total_cost
                    best_solution = [] + ant.tabu
                # update pheromone
                ant._update_pheromone_delta()
            self._update_pheromone(graph, ants)
            # print('generation #{}, best cost: {}, path: {}'.format(gen, best_cost, best_solution))
        return best_solution, best_cost

    def solve_maco(self, graph):
        best_cost = float('inf')
        best_solution = []
        # noinspection PyUnusedLocal
        ants = [Ant(self, graph) for i in range(self.ant_count)]
        for ant in ants:
            for _ in range(graph.city_number - 1):
                ant._select_next_maco()
            ant.total_cost += graph.cost_matrix[ant.tabu[-1]][ant.tabu[0]]
            if ant.total_cost < best_cost:
                best_cost = ant.total_cost
                best_solution = [] + ant.tabu
            # update pheromone
            ant._update_pheromone_delta()

        return self._update_pheromone(graph, ants), best_solution, best_cost
